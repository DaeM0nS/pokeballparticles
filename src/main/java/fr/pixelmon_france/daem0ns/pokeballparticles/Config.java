package fr.pixelmon_france.daem0ns.pokeballparticles;

import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.common.config.Configuration;

import org.apache.logging.log4j.Level;

public class Config {
	
    public static final String CATEGORY_GENERAL = "General";

    public static boolean on = true;

    public static String[] dims = {"0", "72"};

    public static void readConfig() {
        Configuration cfg = PokeballParticles.config;
        try {
            cfg.load();
            initGeneralConfig(cfg);
            
        } catch (Exception e1) {
        	PokeballParticles.logger.log(Level.ERROR, "Problem loading config file!", e1);
        } finally {
            if (cfg.hasChanged()) {
                cfg.save();
            }
        }        
    }

    private static void initGeneralConfig(Configuration cfg) {
        cfg.addCustomCategoryComment(CATEGORY_GENERAL, "General Config.");
        on = cfg.getBoolean("Started", CATEGORY_GENERAL, on, "true if the plugin is enabled, false if the plugin is disabled.");
        dims = cfg.getStringList("Dimensions", CATEGORY_GENERAL, dims, "Set the list of dimensions whitelisted.");
    }
}