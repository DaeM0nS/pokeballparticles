package fr.pixelmon_france.daem0ns.pokeballparticles;

import java.util.List;

import javax.annotation.Nullable;

import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.api.pokemon.ISpecType;
import com.pixelmonmod.pixelmon.api.pokemon.Pokemon;
import com.pixelmonmod.pixelmon.api.pokemon.SpecValue;
import com.pixelmonmod.pixelmon.battles.attacks.Attack;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;

public class PokeballParticlesSpec extends SpecValue<String> implements ISpecType {

    private String value;
    private List<String> keys;

    public PokeballParticlesSpec(List<String> keys, String value){
        super(keys.get(0), value);
        this.keys = keys;
        this.value = value.toLowerCase();
    }

    @Override
    public List<String> getKeys() {
        return this.keys;
    }

    @Override
    public PokeballParticlesSpec parse(@Nullable String s) {
        return new PokeballParticlesSpec(this.keys, s);
    }

    @Override
    public SpecValue<?> readFromNBT(NBTTagCompound nbtTagCompound) {
        return parse(nbtTagCompound.getString(this.key));
    }

    @Override
    public void writeToNBT(NBTTagCompound nbtTagCompound, SpecValue<?> specValue) {
        //nbtTagCompound.setString(this.key, this.value.replaceAll("_"," "));
    }

    @Override
    public Class<? extends SpecValue<?>> getSpecClass() {
        return getClass();
    }

    @Override
    public String toParameterForm(SpecValue<?> specValue) {
        return key + ":" + value;
    }

    @Override
    public Class<String> getValueClass() {
        return String.class;
    }

    @Override
    public void apply(EntityPixelmon entityPixelmon) {
        this.apply(entityPixelmon.getPokemonData());
    }

    @Override
    public void apply(Pokemon pokemon) {
        String effect = this.value;
        if(PokeballParticles.effects.contains(effect)){
            if(!pokemon.getPersistentData().hasKey("pokeballparticles")){
                pokemon.getPersistentData().setTag("pokeballparticles",new NBTTagList());
            }
            pokemon.getPersistentData().getTagList("pokeballparticles",8).appendTag(new NBTTagString(this.key+":"+this.value));
        }
    }

    @Override
    public boolean matches(EntityPixelmon entityPixelmon) {
    	return this.matches(entityPixelmon.getPokemonData());
    }

    @Override
    public boolean matches(NBTTagCompound nbtTagCompound) {
        return this.matches(Pixelmon.pokemonFactory.create(nbtTagCompound));
    }

    @Override
    public boolean matches(Pokemon pokemon) {
    	for(NBTBase n : pokemon.getPersistentData().getTagList("pokeballparticles", 8)) {
    		if(((NBTTagString)n).toString().equals(this.key+":"+this.value)) {
    			return true;
    		}
    	}
        return false;
    }

    @Override
    public SpecValue<String> clone() {
        return new PokeballParticlesSpec(this.keys,this.value);
    }
}
