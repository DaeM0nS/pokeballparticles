package fr.pixelmon_france.daem0ns.pokeballparticles;

import java.io.File;
import java.util.ArrayList;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;

import org.apache.logging.log4j.Logger;

import com.google.common.collect.Lists;
import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.api.pokemon.PokemonSpec;
import com.pixelmonmod.pixelmon.client.particle.ParticleSystems;

import fr.pokepixel.PokeballParticles.commands.PokeballParticlesCommand;
import fr.pokepixel.PokeballParticles.events.Event;

//SERVER SIDE
@Mod(name = "PokeballParticles", modid = "pokeballparticles", version = "1.1.0", acceptedMinecraftVersions="[1.12.2]", dependencies = "required-after:pixelmon", acceptableRemoteVersions="*", serverSideOnly = true)
public class PokeballParticles {
	
    public static Configuration config;

	public static ArrayList<String> effects = new ArrayList<String>();
	public static ArrayList<Integer> dims = new ArrayList<Integer>();
   
	public static Logger logger;
	private void setLogger(Logger logger) {
	    this.logger = logger;
	}
	public static Logger getLogger() {
	    return logger;
	}
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event){
		 config = new Configuration(new File(event.getModConfigurationDirectory(), "PokeballParticles.cfg"));
		 Config.readConfig();
		 
		 PokemonSpec.extraSpecTypes.add(new PokeballParticlesSpec(Lists.newArrayList("pokeballparticles", "pp"), ""));
	}
	
	@EventHandler
	public void Init(FMLInitializationEvent event){
	if(Config.on){
			Pixelmon.EVENT_BUS.register(new Event());
			MinecraftForge.EVENT_BUS.register(new Event());
			for(String d : Config.dims){
				dims.add(Integer.valueOf(d));
			}
			for(ParticleSystems particle : ParticleSystems.values()){
				effects.add(particle.toString().toLowerCase());
			}
			effects.add("bloom:1");
			effects.add("ultrawormhole:1");
			effects.add("helix");
			effects.add("dna");
			effects.add("dragon");
			effects.add("circle");
			effects.add("sphere");
			effects.add("cube");
			effects.add("cube:1");
			effects.add("fountain");
			effects.add("vulcano");
			effects.add("star");
			effects.add("animatedball");
			effects.add("arc");
			effects.add("atom");
			effects.add("cloud");
			effects.add("cone");
			effects.add("cylinder");
			effects.add("discoball");
			effects.add("donut");
			effects.add("earth");
			effects.add("explode");
			effects.add("flame");
			effects.add("grid");
			effects.add("heart");
			effects.add("hill");
			effects.add("love");
			effects.add("music");
			effects.add("shield");
			effects.add("smoke");
			effects.add("tornado");
			effects.add("trace");
			effects.add("vortex");
			effects.add("warp");
			
			effects.add("frost");
			for(String effect : effects) {
				//register permissions kek
				FMLCommonHandler.instance().getMinecraftServerInstance().canUseCommand(0, "pokeballparticles.command.use."+effect);
			}
		 }
	 }

	 @EventHandler
	 public void onServerStart(FMLServerStartingEvent event) {
		 if(Config.on){
	        event.registerServerCommand(new PokeballParticlesCommand());
	        Pixelmon.LOGGER.info("Register PokeballParticles Command.");
		 }
	 }
}
