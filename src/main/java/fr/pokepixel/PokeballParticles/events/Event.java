package fr.pokepixel.PokeballParticles.events;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.SoundEvents;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.WorldServer;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.client.particle.ParticleEffect;
import com.pixelmonmod.pixelmon.client.particle.ParticleSystems;
import com.pixelmonmod.pixelmon.comm.packetHandlers.PlayParticleSystem;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.entities.pixelmon.particleEffects.ParticleEffects;
import com.pixelmonmod.pixelmon.sounds.PixelSounds;

import fr.pixelmon_france.daem0ns.pokeballparticles.PokeballParticles;
import fr.pokepixel.PokeballParticles.utils.Location;
import fr.pokepixel.PokeballParticles.utils.MathUtils;
import fr.pokepixel.PokeballParticles.utils.RandomUtils;
import fr.pokepixel.PokeballParticles.utils.Vector;
import fr.pokepixel.PokeballParticles.utils.VectorUtils;

public class Event {
    /**
     * A group of players who should see this effect.
     */
    public List<EntityPlayerMP> targetPlayers;


	@SubscribeEvent
	public void EntityJoinWorldEvent(EntityJoinWorldEvent event) throws InstantiationException, IllegalAccessException, NumberFormatException, InterruptedException{
		Entity entity = event.getEntity();
		int id = entity.world.getWorldType().getId();
		if(PokeballParticles.dims.contains(id)){
			if(entity instanceof EntityPixelmon){
				EntityPixelmon pixelmon = (EntityPixelmon)entity;
				if(pixelmon.hasOwner()){
					EntityPlayerMP player = FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUUID(pixelmon.getOwnerId());
					if(pixelmon.getEntityData().hasKey("pokeballparticles")) {
						String[] particles = pixelmon.getEntityData().getString("pokeballparticles").split(",");
						for(String particle : particles){
							if(PokeballParticles.effects.contains(particle)){
								WorldServer world = player.getServerWorld();
						    	Location location = new Location(world, pixelmon.posX, pixelmon.posY, pixelmon.posZ);
								switch(particle){
									case "bloom":{
										pixelmon.getEntityWorld().playSound(null, pixelmon.posX, pixelmon.posY, pixelmon.posZ, SoundEvents.BLOCK_ENCHANTMENT_TABLE_USE, SoundCategory.NEUTRAL, 1.0f, 0.1f);
										Pixelmon.network.sendToDimension(new PlayParticleSystem(ParticleSystems.BLOOM, pixelmon.posX, pixelmon.posY, pixelmon.posZ, pixelmon.dimension, pixelmon.getPixelmonScale(), false, new double[0]), pixelmon.dimension);
										break;
									}
									case "bloom:1":{
										pixelmon.getEntityWorld().playSound(null, pixelmon.posX, pixelmon.posY, pixelmon.posZ, SoundEvents.BLOCK_ENCHANTMENT_TABLE_USE, SoundCategory.NEUTRAL, 1.0f, 0.1f);
					                    Pixelmon.network.sendToDimension(new PlayParticleSystem(ParticleSystems.BLOOM, pixelmon.posX, pixelmon.posY, pixelmon.posZ, pixelmon.dimension, pixelmon.getPixelmonScale(), true, new double[0]), pixelmon.dimension);
										break;
									}
									case "discharge":{
										pixelmon.getEntityWorld().playSound(null, pixelmon.posX, pixelmon.posY, pixelmon.posZ, SoundEvents.ENTITY_ENDERMEN_TELEPORT, SoundCategory.NEUTRAL, 1.0f, 0.1f);
					                    
					                    double[] arrd = new double[4];
					                    arrd[0] = pixelmon.getPokemonData().isShiny() ? 0.5 : 1.0;
					                    arrd[1] = 0.5;
					                    arrd[2] = pixelmon.getPokemonData().isShiny() ? 1.0 : 0.5;
					                    arrd[3] = 0.0;
					                    Pixelmon.network.sendToDimension(new PlayParticleSystem(ParticleSystems.DISCHARGE, pixelmon.posX, pixelmon.posY, pixelmon.posZ, pixelmon.dimension, pixelmon.getPixelmonScale(), pixelmon.getPokemonData().isShiny(), arrd), pixelmon.dimension);
										
//										Pixelmon.network.sendToDimension(new PlayParticleSystem(ParticleSystems.DISCHARGE, pixelmon.posX + 0.5, pixelmon.posY - 1, pixelmon.posZ + 0.5, pixelmon.dimension, 0.0f, pixelmon.getPokemonData().isShiny(), 0.0, 0.0, 0.0, 1.0), pixelmon.dimension);
										break;
									}
									case "drain":{
										double[] arrd = new double[3];
				                        arrd[0] = pixelmon.getPokemonData().isShiny() ? 1.0 : 0.5 ;
				                        arrd[1] = 0.5;
				                        arrd[2] = pixelmon.getPokemonData().isShiny() ? 1.0 : 0.5 ;
						                for(int i=0; i<7; i++) {
							                Pixelmon.network.sendToDimension(new PlayParticleSystem(ParticleSystems.DRAIN, pixelmon.posX, pixelmon.posY, pixelmon.posZ, pixelmon.dimension, pixelmon.getPixelmonScale(), pixelmon.getPokemonData().isShiny(), arrd), pixelmon.dimension);
					                        Thread.sleep(50);
						                }
										break;
									}
									case "heal":{
						                Pixelmon.network.sendToDimension(new PlayParticleSystem(ParticleSystems.HEAL, pixelmon.posX, pixelmon.posY, pixelmon.posZ, pixelmon.dimension, pixelmon.getPixelmonScale(), pixelmon.getPokemonData().isShiny(), new double[0]), pixelmon.dimension);
										break;
									}
									case "radialthunder":{
				                        pixelmon.getEntityWorld().playSound(null, pixelmon.posX, pixelmon.posY, pixelmon.posZ, SoundEvents.ENTITY_WITHER_SHOOT, SoundCategory.NEUTRAL, 0.3f, 0.2f);
				                        double[] arrd = new double[3];
				                        arrd[0] = pixelmon.getPokemonData().isShiny() ? 1.0 : 0.5 ;
				                        arrd[1] = 0.5;
				                        arrd[2] = pixelmon.getPokemonData().isShiny() ? 1.0 : 0.5 ;
				                        Pixelmon.network.sendToDimension(new PlayParticleSystem(ParticleSystems.RADIALTHUNDER, pixelmon.posX, pixelmon.posY, pixelmon.posZ, pixelmon.dimension, pixelmon.getPixelmonScale(), pixelmon.getPokemonData().isShiny(), arrd), pixelmon.dimension);
										break;
									}
									case "redchainportail":{
				                        double[] arrd = new double[1];
				                        arrd[0] = pixelmon.getPokemonData().isShiny() ? 1.0 : 0.0 ;
										Pixelmon.network.sendToDimension(new PlayParticleSystem(ParticleSystems.REDCHAINPORTAL, pixelmon.posX, pixelmon.posY, pixelmon.posZ, pixelmon.dimension, pixelmon.getPixelmonScale(), pixelmon.getPokemonData().isShiny(), arrd), pixelmon.dimension);
										break;
									}
									case "slingring":{
						                pixelmon.getEntityWorld().playSound(null, pixelmon.getPosition(), SoundEvents.ENTITY_LIGHTNING_THUNDER, SoundCategory.NEUTRAL, 1.0f, 1.0f);
						                Pixelmon.network.sendToDimension(new PlayParticleSystem(ParticleSystems.SLINGRING, pixelmon.posX, pixelmon.posY, pixelmon.posZ, pixelmon.dimension, pixelmon.getPixelmonScale(), pixelmon.getPokemonData().isShiny(), new double[0]), pixelmon.dimension);
										break;
									}
									case "ultrawormhole":{
						                pixelmon.getEntityWorld().playSound(null, pixelmon.getPosition(), PixelSounds.ultraWormhole, SoundCategory.BLOCKS, 0.5f, 1.0f);
				                        double[] arrd = new double[1];
				                        arrd[0] = 0.5 ;
						                for(int i=0; i<10; i++) {
					                        Pixelmon.network.sendToDimension(new PlayParticleSystem(ParticleSystems.ULTRAWORMHOLE, pixelmon.posX, pixelmon.posY, pixelmon.posZ, pixelmon.dimension, pixelmon.getPixelmonScale(), false, arrd), pixelmon.dimension);
					                        Thread.sleep(50);
						                }
										break;
									}
									case "ultrawormhole:1":{
						                pixelmon.getEntityWorld().playSound(null, pixelmon.getPosition(), PixelSounds.ultraWormhole, SoundCategory.BLOCKS, 0.5f, 1.0f);
				                        double[] arrd = new double[1];
				                        arrd[0] = 1.0 ;
						                for(int i=0; i<10; i++) {
						                	Pixelmon.network.sendToDimension(new PlayParticleSystem(ParticleSystems.ULTRAWORMHOLE, pixelmon.posX, pixelmon.posY, pixelmon.posZ, pixelmon.dimension, pixelmon.getPixelmonScale(), true, arrd), pixelmon.dimension);
					                        Thread.sleep(50);
						                }
										break;
									}
									case "helix": {
										    int radius = 2;
										    for(double y = 0; y <= 50; y+=0.05) {
										        double x = radius * Math.cos(y);
										        double z = radius * Math.sin(y);
										        world.spawnParticle(EnumParticleTypes.FIREWORKS_SPARK, pixelmon.posX + x, pixelmon.posY + y , pixelmon.posZ + z, 1, 0, 0, 0, 0.0, 8);
										    }
										break;
									}
									case "circle": {
									    EnumParticleTypes particlee = EnumParticleTypes.VILLAGER_HAPPY;
									    double xRotation = 0.0;
									    double yRotation = 0.0;
									    double zRotation = 0.0;
									    double angularVelocityX = 0.015707963267948967;
									    double angularVelocityY = 0.018479956785822312;
									    double angularVelocityZ = 0.02026833970057931;
									    float radius = 0.4f * (pixelmon.width+2);
									    float step = 0.0f;
									    double xSubtract;
									    double ySubtract;
									    double zSubtract;
									    boolean enableRotation = true;
									    int particlees = 20;
									    boolean wholeCircle = false;
									    //location.subtract(xSubtract, ySubtract, zSubtract);
									    double inc = 6.283185307179586 / (double)particlees;
									    int steps = particlees ;
									    for (int i = 0; i < steps; ++i) {
									        double angle = (double)step * inc;
									        Vector v = new Vector();
									        v.setX(Math.cos(angle) * (double)radius);
									        v.setZ(Math.sin(angle) * (double)radius);
									        VectorUtils.rotateVector(v, xRotation, yRotation, zRotation);
									        if (enableRotation) {
									            VectorUtils.rotateVector(v, angularVelocityX * (double)step, angularVelocityY * (double)step, angularVelocityZ * (double)step);
									        }
									        world.spawnParticle(particlee, location.clone().add(v).getX(), location.clone().add(v).getY(), location.clone().add(v).getZ(), particlees, 0, 0, 0, 0.0f, 30);
									        step += 1.0f;
									    }
									    break;
									}
									case "dna": {
										EnumParticleTypes particleHelix = EnumParticleTypes.FLAME;
									    EnumParticleTypes particleBase1 = EnumParticleTypes.WATER_WAKE;
									    EnumParticleTypes particleBase2 = EnumParticleTypes.REDSTONE;
									    double radials = Math.PI / 30;
									    float radius = 1.5f;
									    int particlesHelix = 360;
									    int particlesBase = 15;
									    float length = 15;
									    float grow = 0.2f;
									    float baseInterval = 10;
									    int step = 0;
								        for (int j = 0; j < particlesHelix; ++j) {
								            int i;
								            if ((float)step * grow > length) {
								                step = 0;
								            }
								            for (i = 0; i < 2; ++i) {
								                double angle = (double)step * radials + 3.141592653589793 * (double)i;
								                Vector v = new Vector(Math.cos(angle) * (double)radius, (double)((float)step * grow), Math.sin(angle) * (double)radius);
								                drawParticleDNA(location, v, particleHelix, world);
								            }
								            if ((float)step % baseInterval == 0.0f) {
								                for (i = - particlesBase; i <= particlesBase; ++i) {
								                    if (i == 0) continue;
								                    EnumParticleTypes particlee = particleBase1;
								                    //Color color = colorBase1;
								                    
								                    if (i < 0) {
								                        particlee = particleBase2;
								                    //    color = colorBase2;
								                    }
								                    double angle = (double)step * radials;
								                    Vector v = new Vector(Math.cos(angle), 0.0, Math.sin(angle)).multiply(radius * (float)i / (float)particlesBase).setY((float)step * grow);
								                    drawParticleDNA(location, v, particlee, world);
								                }
								            }
								            ++step;
								        }
										break;
									}
									case "dragon":{
									    final List<Float> rndF;
									    final List<Double> rndAngle;
										EnumParticleTypes particlee = EnumParticleTypes.FLAME;
									    int arcs = 20;
									    float pitch = 0.1f;
									    int particlees = 30;
									    int stepsPerIteration = 2;
									    float length = 4.0f;
									    int step = 0;
								        int iterations = 200;
								        rndF = new ArrayList<Float>(arcs);
								        rndAngle = new ArrayList<Double>(arcs);
									    while(step<iterations){
											for (int j = 0; j < stepsPerIteration; ++j) {
									            if (step % particlees == 0) {
									                rndF.clear();
									                rndAngle.clear();
									            }
									            while (rndF.size() < arcs) {
									                rndF.add(Float.valueOf(RandomUtils.random.nextFloat()));
									            }
									            while (rndAngle.size() < arcs) {
									                rndAngle.add(RandomUtils.getRandomAngle());
									            }
									            for (int i = 0; i < arcs; ++i) {
									                pitch = rndF.get(i).floatValue() * 2.0f * pitch - pitch;
									                float x = (float)(step % particlees) * length / (float)particlees;
									                float y = (float)((double)pitch * Math.pow(x, 2.0));
									                Vector v = new Vector(x, y, 0.0f);
									                VectorUtils.rotateAroundAxisX(v, rndAngle.get(i));
									                VectorUtils.rotateAroundAxisZ(v, (- location.getPitch()) * 0.017453292f);
									                VectorUtils.rotateAroundAxisY(v, (- location.getYaw() + 90.0f) * 0.017453292f);
									                VectorUtils.rotateAroundAxisX(v, 90.0);
									                world.spawnParticle(particlee, location.add(v).getX(), location.add(v).getY(), location.add(v).getZ(), 30, 0, 0, 0, 0.0);
									                location.subtract(v);
									            }
									        }
									    ++step;
										}
										break;
									}
									case "sphere":{
									    EnumParticleTypes particlee = EnumParticleTypes.SMOKE_LARGE;
									    double radius = 1.0;
									    double yOffset = 0.0;
									    int particlees = 50;
									    double radiusIncrease = 0.0;
								        if (radiusIncrease != 0.0) {
								            radius += radiusIncrease;
								        }
								        location.add(0.0, yOffset, 0.0);
								        for (int i = 0; i < particlees; ++i) {
								            Vector vector = RandomUtils.getRandomVector().multiply(radius);
								            location.add(vector);
								            world.spawnParticle(particlee, location.getX(), location.getY(), location.getZ(), 50, 0, 0, 0, 0.0, 0);
								            location.subtract(vector);
								        }
								        break;
									}
									case "cube": {
									    EnumParticleTypes particlee = EnumParticleTypes.FLAME;
									    float edgeLength = 3.0f;
									    double angularVelocityX = 0.015707963267948967;
									    double angularVelocityY = 0.018479956785822312;
									    double angularVelocityZ = 0.02026833970057931;
									    int particlees = 8;
									    boolean enableRotation = true;
									    boolean outlineOnly = true;
									    int step = 0;			
									    int iterations = 200;
								        double xRotation = 0.0;
								        double yRotation = 0.0;
								        double zRotation = 0.0;
									    while(step<iterations){

								        if (enableRotation) {
								            xRotation = (double)step * angularVelocityX;
								            yRotation = (double)step * angularVelocityY;
								            zRotation = (double)step * angularVelocityZ;
								        }
								        float a = edgeLength / 2.0f;
								        Vector v = new Vector();
								        for (int i = 0; i < 4; ++i) {
								            double angleY = (double)i * 3.141592653589793 / 2.0;
								            for (int j = 0; j < 2; ++j) {
								                double angleX = (double)j * 3.141592653589793;
								                for (int p = 0; p <= particlees; ++p) {
								                    v.setX(a).setY(a);
								                    v.setZ(edgeLength * (float)p / (float)particlees - a);
								                    VectorUtils.rotateAroundAxisX(v, angleX);
								                    VectorUtils.rotateAroundAxisY(v, angleY);
								                    if (enableRotation) {
								                        VectorUtils.rotateVector(v, xRotation, yRotation, zRotation);
								                    }
										            world.spawnParticle(particlee, location.add(v).getX(), location.add(v).getY(), location.add(v).getZ(), 8, 0, 0, 0, 0.0, 0);
								                    //display(particle, location.add(v));
								                    location.subtract(v);
								                }
								            }
								            for (int p = 0; p <= particlees; ++p) {
								                v.setX(a).setZ(a);
								                v.setY(edgeLength * (float)p / (float)particlees - a);
								                VectorUtils.rotateAroundAxisY(v, angleY);
								                if (enableRotation) {
								                    VectorUtils.rotateVector(v, xRotation, yRotation, zRotation);
								                }
									            world.spawnParticle(particlee, location.add(v).getX(), location.add(v).getY(), location.add(v).getZ(), 1, 0, 0, 0, 0.0, 50);
									            //display(particle, location.add(v));
								                location.subtract(v);
								            }
								        }
								    
								        ++step;
									}
								        break;
									}
									case "cube:1": {
									    EnumParticleTypes particlee = EnumParticleTypes.FLAME;
									    float edgeLength = 3.0f;
									    double angularVelocityX = 0.015707963267948967;
									    double angularVelocityY = 0.018479956785822312;
									    double angularVelocityZ = 0.02026833970057931;
									    int particlees = 8;
									    boolean enableRotation = true;
									    boolean outlineOnly = true;
									    int step = 0;	
									    int iterations = 200;
								        double xRotation = 0.0;
								        double yRotation = 0.0;
								        double zRotation = 0.0;
									    while(step<iterations){

								        if (enableRotation) {
								            xRotation = (double)step * angularVelocityX;
								            yRotation = (double)step * angularVelocityY;
								            zRotation = (double)step * angularVelocityZ;
								        }
								        float a = edgeLength / 2.0f;
								        for (int x = 0; x <= particlees; ++x) {
								            float posX = edgeLength * ((float)x / (float)particlees) - a;
								            for (int y = 0; y <= particlees; ++y) {
								                float posY = edgeLength * ((float)y / (float)particlees) - a;
								                for (int z = 0; z <= particlees; ++z) {
								                    if (x != 0 && x != particlees && y != 0 && y != particlees && z != 0 && z != particlees) continue;
								                    float posZ = edgeLength * ((float)z / (float)particlees) - a;
								                    Vector v = new Vector(posX, posY, posZ);
								                    if (enableRotation) {
								                        VectorUtils.rotateVector(v, xRotation, yRotation, zRotation);
								                    }
										            world.spawnParticle(particlee, location.add(v).getX(), location.add(v).getY(), location.add(v).getZ(), 1, 0, 0, 0, 0.0, 50);
										            //display(particle, location.add(v));
								                    location.subtract(v);
								                }
								            }
								        }
								        ++step;
									    }
										break;
									}
									case "fountain": {
									    EnumParticleTypes particlee = EnumParticleTypes.WATER_SPLASH;
									    int strands = 10;
									    int particlesStrand = 150;
									    int particlesSpout = 200;
									    float radius = 5.0f;
									    float radiusSpout = 0.1f;
									    float height = 3.0f;
									    float heightSpout = 7.0f;
									    double rotation = 0.7853981633974483;
									    int i;
									    for (i = 1; i <= strands; ++i) {
									        double angle = (double)(2 * i) * 3.141592653589793 / (double)strands + rotation;
									        for (int j = 1; j <= particlesStrand; ++j) {
									            float ratio = (float)j / (float)particlesStrand;
									            double x = Math.cos(angle) * (double)radius * (double)ratio;
									            double y = Math.sin(3.141592653589793 * (double)j / (double)particlesStrand) * (double)height;
									            double z = Math.sin(angle) * (double)radius * (double)ratio;
									            location.add(x, y, z);
									            world.spawnParticle(particlee, location.getX(), location.getY(), location.getZ(), particlesStrand, 0, 0, 0, 0.0);
//									            display(particle, location);
									            location.subtract(x, y, z);
									        }
									    }
									    for (i = 0; i < particlesSpout; ++i) {
									        Vector v = RandomUtils.getRandomCircleVector().multiply(RandomUtils.random.nextFloat() * radius * radiusSpout);
									        v.setY(RandomUtils.random.nextFloat() * heightSpout);
									        location.add(v);
								            world.spawnParticle(particlee, location.getX(), location.getY(), location.getZ(), particlesSpout, 0, 0, 0, 0.0);
									        //display(particle, location);
									        location.subtract(v);
									    }
										break;
									}
									case "vulcano": {
									    EnumParticleTypes particlee = EnumParticleTypes.LAVA;
									    EnumParticleTypes particleee = EnumParticleTypes.FIREWORKS_SPARK;
									    int strands = 10;
									    int particlesStrand = 150;
									    int particlesSpout = 200;
									    float radius = 5.0f;
									    float radiusSpout = 0.1f;
									    float height = 3.0f;
									    float heightSpout = 7.0f;
									    double rotation = 0.7853981633974483;

									    int i;
									    for (i = 1; i <= strands; ++i) {
									        double angle = (double)(2 * i) * 3.141592653589793 / (double)strands + rotation;
									        for (int j = 1; j <= particlesStrand; ++j) {
									            float ratio = (float)j / (float)particlesStrand;
									            double x = Math.cos(angle) * (double)radius * (double)ratio;
									            double y = Math.sin(3.141592653589793 * (double)j / (double)particlesStrand) * (double)height;
									            double z = Math.sin(angle) * (double)radius * (double)ratio;
									            location.add(x, y, z);
									            world.spawnParticle(particleee, location.getX(), location.getY(), location.getZ(), particlesStrand, 0, 0, 0, 0.0);
//									            display(particle, location);
									            location.subtract(x, y, z);
									        }
									    }
									    for (i = 0; i < particlesSpout; ++i) {
									        Vector v = RandomUtils.getRandomCircleVector().multiply(RandomUtils.random.nextFloat() * radius * radiusSpout);
									        v.setY(RandomUtils.random.nextFloat() * heightSpout);
									        location.add(v);
								            world.spawnParticle(particlee, location.getX(), location.getY(), location.getZ(), particlesSpout, 0, 0, 0, 0.0);
									        //display(particle, location);
									        location.subtract(v);
									    }
										break;
									}
									case "star": {
									    EnumParticleTypes particlee = EnumParticleTypes.FLAME;
									    int particlees = 50;
									    float spikeHeight = 3.5f;
									    int spikesHalf = 5;
									    float innerRadius = 0.5f;
								        float radius = 3.0f * innerRadius / 1.73205f;
								        for (int i = 0; i < spikesHalf * 2; ++i) {
								            double xRotation = (double)i * 3.141592653589793 / (double)spikesHalf;
								            for (int x = 0; x < particlees; ++x) {
								                double angle = 6.283185307179586 * (double)x / (double)particlees;
								                float height = RandomUtils.random.nextFloat() * spikeHeight;
								                Vector v = new Vector(Math.cos(angle), 0.0, Math.sin(angle));
								                v.multiply((spikeHeight - height) * radius / spikeHeight);
								                v.setY(innerRadius + height);
								                VectorUtils.rotateAroundAxisX(v, xRotation);
								                location.add(v);
									            world.spawnParticle(particlee, location.getX(), location.getY(), location.getZ(), particlees, 0, 0, 0, 0.0);
								                //display(particle, location);
								                location.subtract(v);
								                VectorUtils.rotateAroundAxisX(v, 3.141592653589793);
								                VectorUtils.rotateAroundAxisY(v, 1.5707963267948966);
								                location.add(v);
									            world.spawnParticle(particlee, location.getX(), location.getY(), location.getZ(), particlees, 0, 0, 0, 0.0);
									            //display(particle, location);
								                location.subtract(v);
								            }
								        }
										break;
									}
									case "animatedball" :{
									    EnumParticleTypes particlee = EnumParticleTypes.SPELL;
									    int particlees = 150;
									    int particlesPerIteration = 10;
									    float size = 1.0f;
									    float xFactor = 1.0f;
									    float yFactor = 2.0f;
									    float zFactor = 1.0f;
									    float xOffset = 0.0f;
									    float yOffset = 0.8f;
									    float zOffset = 0.0f;
									    double xRotation = 0.0;
									    double yRotation = 0.0;
									    double zRotation = 0.0;
									    int step = 0;
									    for(int j=0; j<500; j++){
									        Vector vector = new Vector();
									        for (int i = 0; i < particlesPerIteration; ++i) {
									            ++step;
									            float t = 3.1415927f / (float)particlees * (float)step;
									            float r = MathUtils.sin(t) * size;
									            float s = 6.2831855f * t;
									            vector.setX(xFactor * r * MathUtils.cos(s) + xOffset);
									            vector.setZ(zFactor * r * MathUtils.sin(s) + zOffset);
									            vector.setY(yFactor * size * MathUtils.cos(t) + yOffset);
									            VectorUtils.rotateVector(vector, xRotation, yRotation, zRotation);
									            world.spawnParticle(particlee, location.add(vector).getX(), location.add(vector).getY(), location.add(vector).getZ(), particlees, 0, 0, 0, 0.0);
									            //display(particlee, location.add(vector));
									            location.subtract(vector);
									        }
									    }								    
										break;
									}
									case "arc": {
									    EnumParticleTypes particlee = EnumParticleTypes.FLAME;
									    float height = 2.0f;
									    int particlees = 100;
									    int step = 0;
								        Location target = new Location(world, pixelmon.posX, pixelmon.posY+10, pixelmon.posZ);
								        Vector link = target.toVector().subtract(location.toVector());
								        float length = (float)link.length();
								        float pitch = (float)((double)(4.0f * height) / Math.pow(length, 2.0));
								        for (int i = 0; i < particlees; ++i) {
								            Vector v = link.clone().normalize().multiply(length * (float)i / (float)particlees);
								            float x = (float)i / (float)particlees * length - length / 2.0f;
								            float y = (float)((double)(- pitch) * Math.pow(x, 2.0) + (double)height);
								            location.add(v).add(0.0, (double)y, 0.0);
								            world.spawnParticle(particlee, location.getX(), location.getY(), location.getZ(), particlees, 0, 0, 0, 0.0);
								            //display(particle, location);
								            location.subtract(0.0, (double)y, 0.0).subtract(v);
								            ++step;
								        }
										break;
									}
									case "atom": {
										EnumParticleTypes particleNucleus = EnumParticleTypes.DRIP_WATER;
									    EnumParticleTypes particleOrbital = EnumParticleTypes.DRIP_LAVA;
									    double radius = 3.0;
									    float radiusNucleus = 0.2f;
									    int particlesNucleus = 10;
									    int particlesOrbital = 10;
									    int orbitals = 3;
									    double rotation = 0.0;
									    double angularVelocity = 0.039269908169872414;
									    int step = 0;
									    while(step<200){
										    for (int i = 0; i < particlesNucleus; ++i) {
										        Vector v = RandomUtils.getRandomVector().multiply(radius * (double)radiusNucleus);
										        location.add(v);
									            world.spawnParticle(particleNucleus, location.getX(), location.getY(), location.getZ(), particlesNucleus, 0, 0, 0, 0.0);
										        //display(particleNucleus, location);
										        location.subtract(v);
										    }
										    for (int i = 0; i < particlesOrbital; ++i) {
										        double angle = (double)step * angularVelocity;
										        for (int j = 0; j < orbitals; ++j) {
										            double xRotation = 3.141592653589793 / (double)orbitals * (double)j;
										            Vector v = new Vector(Math.cos(angle), Math.sin(angle), 0.0).multiply(radius);
										            VectorUtils.rotateAroundAxisX(v, xRotation);
										            VectorUtils.rotateAroundAxisY(v, rotation);
										            location.add(v);
										            world.spawnParticle(particleOrbital, location.getX(), location.getY(), location.getZ(), particlesOrbital, 0, 0, 0, 0.0);
										            //display(particleOrbital, location, colorOrbital);
										            location.subtract(v);
										        }
										        ++step;
										    }
									    }
										break;
									}
									case "cloud": {
										EnumParticleTypes cloudParticle = EnumParticleTypes.CLOUD;
									    EnumParticleTypes mainParticle = EnumParticleTypes.DRIP_WATER;
									    float cloudSize = 0.7f;
									    float particleRadius = cloudSize - 0.1f;
									    double yOffset = 0.8;
										location.add(0.0, yOffset, 0.0);
								        for (int i = 0; i < 50; ++i) {
								            Vector v = RandomUtils.getRandomCircleVector().multiply(RandomUtils.random.nextDouble() * (double)cloudSize);
								            world.spawnParticle(cloudParticle, location.add(v).getX(), location.add(v).getY(), location.add(v).getZ(), 7, 0, 0, 0, 0.0);
								            //display(cloudParticle, location.add(v), 0.0f, 7);
								            location.subtract(v);
								        }
								        Location l = location.add(0.0, 0.2, 0.0);
								        for (int i = 0; i < 15; ++i) {
								            int r = RandomUtils.random.nextInt(2);
								            double x = RandomUtils.random.nextDouble() * (double)particleRadius;
								            double z = RandomUtils.random.nextDouble() * (double)particleRadius;
								            l.add(x, 0.0, z);
								            if (r != 1) {
									            //world.spawnParticle(mainParticle, l.getX(), l.getY(), l.getZ(), particlesOrbital, 0, 0, 0, 0.0);
									            world.spawnParticle(mainParticle, l.getX(), l.getY(), l.getZ(), 7, 0, 0, 0, 0.0);
									            //display(mainParticle, l);
								            }
								            l.subtract(x, 0.0, z);
								            l.subtract(x, 0.0, z);
								            if (r != 1) {
									            world.spawnParticle(mainParticle, l.getX(), l.getY(), l.getZ(), 7, 0, 0, 0, 0.0);
								                //display(mainParticle, l);
								            }
								            l.add(x, 0.0, z);
								        }
										break;
							        }
									case "cone": {
										EnumParticleTypes particlee = EnumParticleTypes.FLAME;
									    float lengthGrow = 0.05f;
									    double angularVelocity = 0.19634954084936207;
									    int particlees = 10;
									    float radiusGrow = 0.006f;
									    int particlesCone = 180;
									    double rotation = 0.0;
									    boolean randomize = false;
									    int step = 0;
									    while(step<200){
									        for (int x = 0; x < particlees; ++x) {
									            if (randomize && step == 0) {
									                rotation = RandomUtils.getRandomAngle();
									            }
									            double angle = (double)step * angularVelocity + rotation;
									            float radius = (float)step * radiusGrow;
									            float length = (float)step * lengthGrow;
									            Vector v = new Vector(Math.cos(angle) * (double)radius, (double)length, Math.sin(angle) * (double)radius);
									            VectorUtils.rotateAroundAxisX(v, (location.getPitch() + 90.0f) * 0.017453292f);
									            VectorUtils.rotateAroundAxisY(v, (- location.getYaw()) * 0.017453292f);
									            location.add(v);
									            world.spawnParticle(particlee, location.getX(), location.getY(), location.getZ(), particlees, 0, 0, 0, 0.0);
									            //display(particle, location);
									            location.subtract(v);
									            ++step;
										    }
								        }
										break;
									}
									case "cylinder": {
										EnumParticleTypes particlee = EnumParticleTypes.FLAME;
								        float radius = 1.0f;
								        float height = 3.0f;
								        double angularVelocityX = 0.015707963267948967;
								        double angularVelocityY = 0.018479956785822312;
								        double angularVelocityZ = 0.02026833970057931;
								        double rotationX = 0.0;
								        double rotationY = 0.0;
								        double rotationZ = 0.0;
								        int particlees = 100;
								        boolean enableRotation = true;
								        boolean solid = false;
								        int step = 0;
								        float sideRatio = 0.0f;
								        while(step<200){
								        if (sideRatio == 0.0f) {
								            calculateSideRatio(radius, height, sideRatio);
								        }
								        Random r = RandomUtils.random;
								        double xRotation = rotationX;
								        double yRotation = rotationY;
								        double zRotation = rotationZ;
								        if (enableRotation) {
								            xRotation += (double)step * angularVelocityX;
								            yRotation += (double)step * angularVelocityY;
								            zRotation += (double)step * angularVelocityZ;
								        }
								        for (int i = 0; i < particlees; ++i) {
								            float multi = solid ? r.nextFloat() : 1.0f;
								            Vector v = RandomUtils.getRandomCircleVector().multiply(radius);
								            if (r.nextFloat() <= sideRatio) {
								                v.multiply(multi);
								                v.setY((r.nextFloat() * 2.0f - 1.0f) * (height / 2.0f));
								            } else {
								                v.multiply(r.nextFloat());
								                if ((double)r.nextFloat() < 0.5) {
								                    v.setY(multi * (height / 2.0f));
								                } else {
								                    v.setY((- multi) * (height / 2.0f));
								                }
								            }
								            if (enableRotation) {
								                VectorUtils.rotateVector(v, xRotation, yRotation, zRotation);
								            }
								            world.spawnParticle(particlee, location.add(v).getX(), location.add(v).getY(), location.add(v).getZ(), particlees, 0, 0, 0, 0.0);
								            //display(particle, location.add(v));
								            location.subtract(v);
								        }
							            world.spawnParticle(particlee, location.getX(), location.getY(), location.getZ(), particlees, 0, 0, 0, 0.0);
								        //display(particle, location);
								        ++step;
								        }
										break;
									}
									case "discoball": {
									    float sphereRadius = 0.6f;
									    int max = 15;
									    EnumParticleTypes sphereParticle = EnumParticleTypes.FLAME;
									    EnumParticleTypes lineParticle = EnumParticleTypes.REDSTONE;
									    int maxLines = 7;
									    int lineParticles = 100;
									    int sphereParticles = 50;
									    Direction direction = Direction.DOWN;
									    int mL = RandomUtils.random.nextInt(maxLines - 2) + 2;
								        for (int m = 0; m < mL * 2; ++m) {
								            double x = RandomUtils.random.nextInt(max - max * -1) + max * -1;
								            double y = RandomUtils.random.nextInt(max - max * -1) + max * -1;
								            double z = RandomUtils.random.nextInt(max - max * -1) + max * -1;
								            if (direction == Direction.DOWN) {
								                y = RandomUtils.random.nextInt(max * 2 - max) + max;
								            } else if (direction == Direction.UP) {
								                y = RandomUtils.random.nextInt(max * -1 - max * -2) + max * -2;
								            }
								            Location target = location.clone().subtract(x, y, z);
								            Vector link = target.toVector().subtract(location.toVector());
								            float length = (float)link.length();
								            link.normalize();
								            float ratio = length / (float)lineParticles;
								            Vector v = link.multiply(ratio);
								            Location loc = location.clone().subtract(v);
								            for (int i = 0; i < lineParticles; ++i) {
								                loc.add(v);
									            world.spawnParticle(lineParticle, loc.getX(), loc.getY(), loc.getZ(), lineParticles, 0, 0, 0, 0.0);
								                //display(lineParticle, loc);
								            }
								        }
								        for (int i = 0; i < sphereParticles; ++i) {
								            Vector vector = RandomUtils.getRandomVector().multiply(sphereRadius);
								            location.add(vector);
								            world.spawnParticle(sphereParticle, location.getX(), location.getY(), location.getZ(), sphereParticles, 0, 0, 0, 0.0);
								            //display(sphereParticle, location);
								            location.subtract(vector);
								        }
										break;
									}
									case "donut":{
										EnumParticleTypes particlee = EnumParticleTypes.FLAME;
								        int particlesCircle = 10;
								        int circles = 36;
								        float radiusDonut = 2.0f;
								        float radiusTube = 0.5f;
								        double xRotation = 0.0;
								        double yRotation = 0.0;
								        double zRotation = 0.0;
								        Vector v = new Vector();
								        for (int i = 0; i < circles; ++i) {
								            double theta = 6.283185307179586 * (double)i / (double)circles;
								            for (int j = 0; j < particlesCircle; ++j) {
								                double phi = 6.283185307179586 * (double)j / (double)particlesCircle;
								                double cosPhi = Math.cos(phi);
								                v.setX(((double)radiusDonut + (double)radiusTube * cosPhi) * Math.cos(theta));
								                v.setY(((double)radiusDonut + (double)radiusTube * cosPhi) * Math.sin(theta));
								                v.setZ((double)radiusTube * Math.sin(phi));
								                VectorUtils.rotateVector(v, xRotation, yRotation, zRotation);
									            world.spawnParticle(particlee, location.add(v).getX(), location.add(v).getY(), location.add(v).getZ(), particlesCircle, 0, 0, 0, 0.0);
								                //display(particlee, location.add(v));
								                location.subtract(v);
								            }
								        }
										break;
									}
									case "earth": {
										EnumParticleTypes particle1 = EnumParticleTypes.VILLAGER_HAPPY;
										EnumParticleTypes particle2 = EnumParticleTypes.DRIP_WATER;
									    int precision = 100;
									    int particlees = 500;
									    float radius = 1.0f;
									    float mountainHeight = 0.5f;
									    boolean firstStep = true;
									    final Set<Vector> cacheGreen = new HashSet<Vector>();
									    final Set<Vector> cacheBlue = new HashSet<Vector>();
								        if (firstStep) {
								            invalidate(firstStep, cacheGreen, cacheBlue, particlees, radius, mountainHeight);
								        }
								        for (Vector v : cacheGreen) {
								            world.spawnParticle(particle1, location.add(v).getX(), location.add(v).getY(), location.add(v).getZ(), 3, 0, 0, 0, 0.0);
								            //display(particle1, location.add(v), 0.0f, 3);
								            location.subtract(v);
								        }
								        for (Vector v : cacheBlue) {
								            world.spawnParticle(particle2, location.add(v).getX(), location.add(v).getY(), location.add(v).getZ(), particlees, 0, 0, 0, 0.0);
								            //display(particle2, location.add(v));
								            location.subtract(v);
								        }
										break;
									}
									case "explode": {
										EnumParticleTypes particle1 = EnumParticleTypes.EXPLOSION_NORMAL;
										EnumParticleTypes particle2 = EnumParticleTypes.EXPLOSION_HUGE;
								        int amount = 25;
								        SoundEvent sound = SoundEvents.ENTITY_GENERIC_EXPLODE;
						                world.playSound(null, pixelmon.getPosition(), sound, SoundCategory.BLOCKS, 0.5f, 1.0f);
						                world.spawnParticle(particle1, location.getX(), location.getY(), location.getZ(), 25, 0, 0, 0, 5.0);
								        //display(this.particle1, location);
							            world.spawnParticle(particle2, location.getX(), location.getY(), location.getZ(), 25, 0, 0, 0, 5.0);
								        //display(this.particle2, location);
										break;
									}
									case "flame":{
										EnumParticleTypes particlee = EnumParticleTypes.FLAME;
								        for (int i = 0; i < 10; ++i) {
								            Vector v = RandomUtils.getRandomCircleVector().multiply(RandomUtils.random.nextDouble() * 0.6);
								            v.setY((double)RandomUtils.random.nextFloat() * 1.8);
								            location.add(v);
							                world.spawnParticle(particlee, location.getX(), location.getY(), location.getZ(), 25, 0, 0, 0, 0.0);
								            //display(particle, location);
								            location.subtract(v);
								        }
										break;
									}
									case "grid": {
									    EnumParticleTypes particlee = EnumParticleTypes.FLAME;
									    int rows = 5;
									    int columns = 10;
									    float widthCell = 1.0f;
									    float heightCell = 1.0f;
									    int particlesWidth = 4;
									    int particlesHeight = 3;
									    double rotation = 0.0;
									    int j;
								        int i;
								        Vector v = new Vector();
								        for (i = 0; i <= rows + 1; ++i) {
								            for (j = 0; j < particlesWidth * (columns + 1); ++j) {
								                v.setY((float)i * heightCell);
								                v.setX((float)j * widthCell / (float)particlesWidth);
								                addParticle(location, v, rotation, particlee, world);
								            }
								        }
								        for (i = 0; i <= columns + 1; ++i) {
								            for (j = 0; j < particlesHeight * (rows + 1); ++j) {
								                v.setX((float)i * widthCell);
								                v.setY((float)j * heightCell / (float)particlesHeight);
								                addParticle(location, v, rotation, particlee, world);
								            }
								        }
										break;
									}
									case "heart": {
										EnumParticleTypes particlee = EnumParticleTypes.CRIT_MAGIC;
									    int particlees = 50;
									    double xRotation = 0.0;
									    double yRotation = 0.0;
									    double zRotation = 0.0;
									    double yFactor = 1.0;
									    double xFactor = 1.0;
									    double factorInnerSpike = 0.8;
									    double compressYFactorTotal = 2.0;
									    float compilaction = 2.0f;
								        Vector vector = new Vector();
								        for (int i = 0; i < particlees; ++i) {
								            float alpha = 3.1415927f / compilaction / (float)particlees * (float)i;
								            double phi = Math.pow((double)Math.abs(MathUtils.sin(2.0f * compilaction * alpha)) + factorInnerSpike * (double)Math.abs(MathUtils.sin(compilaction * alpha)), 1.0 / compressYFactorTotal);
								            vector.setY(phi * (double)(MathUtils.sin(alpha) + MathUtils.cos(alpha)) * yFactor);
								            vector.setZ(phi * (double)(MathUtils.cos(alpha) - MathUtils.sin(alpha)) * xFactor);
								            VectorUtils.rotateVector(vector, xRotation, yRotation, zRotation);
							                world.spawnParticle(particlee, location.add(vector).getX(), location.add(vector).getY(), location.add(vector).getZ(), particlees, 0, 0, 0, 0.0);
							                //display(particlee, location.add(vector));
								            location.subtract(vector);
								        }
										break;
									}
									case "hill": {
										EnumParticleTypes particlee = EnumParticleTypes.FLAME;
									    float height = 2.5f;
									    float particlees = 30.0f;
									    float edgeLength = 6.5f;
									    double yRotation = 0.4487989505128276;
									    Vector v = new Vector();
									    int x = 0;
									    while ((float)x <= particlees) {
									        double y1 = Math.sin(3.141592653589793 * (double)x / (double)particlees);
									        int z = 0;
									        while ((float)z <= particlees) {
									            double y2 = Math.sin(3.141592653589793 * (double)z / (double)particlees);
									            v.setX(edgeLength * (float)x / particlees).setZ(edgeLength * (float)z / particlees);
									            v.setY((double)height * y1 * y2);
									            VectorUtils.rotateAroundAxisY(v, yRotation);
									            world.spawnParticle(particlee, location.add(v).getX(), location.add(v).getY(), location.add(v).getZ(), 30, 0, 0, 0, 0.0);
									            //this.display(particle, location.add(v));
									            location.subtract(v);
									            ++z;
									        }
									        ++x;
									    }
										break;
									}
									case "love": {
										EnumParticleTypes particlee = EnumParticleTypes.HEART;
									    location.add(RandomUtils.getRandomCircleVector().multiply(RandomUtils.random.nextDouble() * 0.6));
									    location.add(0.0, (double)(RandomUtils.random.nextFloat() * 2.0f), 0.0);
						                world.spawnParticle(particlee, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, 0.0);
						                //this.display(this.particle, location);									}
										break;
									}
									case "music": {
										EnumParticleTypes particlee = EnumParticleTypes.NOTE;
									    double radialsPerStep = 0.39269908169872414;
									    float radius = 0.4f;
									    float step = 0.0f;
									    location.add(0.0, 1.899999976158142, 0.0);
								        location.add(Math.cos(radialsPerStep * (double)step) * (double)radius, 0.0, Math.sin(radialsPerStep * (double)step) * (double)radius);
						                world.spawnParticle(particlee, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, 0.0);
								        //display(particlee, location);
								        step += 1.0f;
										break;
									}
									case "shield":{
										EnumParticleTypes particlee = EnumParticleTypes.FLAME;
									    double radius = 3.0;
									    int particlees = 50;
									    boolean sphere = false;
										for (int i = 0; i < particlees; ++i) {
								            Vector vector = RandomUtils.getRandomVector().multiply(radius);
								            if (!sphere) {
								                vector.setY(Math.abs(vector.getY()));
								            }
								            location.add(vector);
							                world.spawnParticle(particlee, location.getX(), location.getY(), location.getZ(), particlees, 0, 0, 0, 0.0);
							                //this.display(particle, location);
								            location.subtract(vector);
								        }
										break;
									}
									case "smoke": {
										EnumParticleTypes particlee = EnumParticleTypes.SMOKE_NORMAL;
									    for (int i = 0; i < 20; ++i) {
									        location.add(RandomUtils.getRandomCircleVector().multiply(RandomUtils.random.nextDouble() * 0.6));
									        location.add(0.0, (double)(RandomUtils.random.nextFloat() * 2.0f), 0.0);
							                world.spawnParticle(particlee, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, 0.0);
									        //this.display(this.particle, location);
									    }
										break;
									}
									case "tornado": {
										EnumParticleTypes tornadoParticle = EnumParticleTypes.FLAME;
										EnumParticleTypes cloudParticle = EnumParticleTypes.CLOUD;
									    float cloudSize = 2.5f;
									    double yOffset = 0.8;
									    float tornadoHeight = 5.0f;
									    float maxTornadoRadius = 5.0f;
									    boolean showCloud = true;
									    boolean showTornado = true;
									    double distance = 0.375;
									    int step = 0;
									    Location l = location.add(0.0, yOffset, 0.0);
								        int i = 0;
								        while ((float)i < 100.0f * cloudSize) {
								            Vector v = RandomUtils.getRandomCircleVector().multiply(RandomUtils.random.nextDouble() * (double)cloudSize);
								            if (showCloud) {
								                world.spawnParticle(tornadoParticle, l.add(v).getX(), l.add(v).getY(), l.add(v).getZ(), 7, 0, 0, 0, 0.0);
								                //display(cloudParticle, l.add(v), 0.0f, 7);
								                l.subtract(v);
								            }
								            ++i;
								        }
								        Location t = l.clone().add(0.0, 0.2, 0.0);
								        double r = 0.45 * ((double)maxTornadoRadius * (2.35 / (double)tornadoHeight));
								        for (double y = 0.0; y < (double)tornadoHeight; y += distance) {
								            double fr = r * y;
								            if (fr > (double)maxTornadoRadius) {
								                fr = maxTornadoRadius;
								            }
								            for (Vector v : createCircle(y, fr)) {
								                if (!showTornado) continue;
								                world.spawnParticle(tornadoParticle, t.add(v).getX(), t.add(v).getY(), t.add(v).getZ(), 1, 0, 0, 0, 0.0);
								                //display(tornadoParticle, t.add(v));
								                t.subtract(v);
								                ++step;
								            }
								        }
								        l.subtract(0.0, yOffset, 0.0);
										break;
									}
									case "trace": {
										EnumParticleTypes particlee = EnumParticleTypes.FLAME;
									    int refresh = 5;
									    int maxWayPoints = 30;
									    final List<Vector> wayPoints = new ArrayList<Vector>();
									    int step = 0;
								        List<Vector> list = wayPoints;
								        synchronized (list) {
								            if (wayPoints.size() >= maxWayPoints) {
								                wayPoints.remove(0);
								            }
								        }
								        wayPoints.add(location.toVector());
								        ++step;
								        if (step % refresh != 0) {
								            return;
								        }
								        list = wayPoints;
								        synchronized (list) {
								            for (Vector position : wayPoints) {
								                Location particleLocation = new Location(world, position.getX(), position.getY(), position.getZ());
								                world.spawnParticle(particlee, particleLocation.getX(), particleLocation.getY(), particleLocation.getZ(), 1, 0, 0, 0, 0.0);
								                //display(particlee, particleLocation);
								            }
								        }
										break;
									}
									case "vortex": {
										EnumParticleTypes particlee = EnumParticleTypes.FLAME;
									    float radius = 2.0f;
									    float grow = 0.05f;
									    double radials = 0.19634954084936207;
									    int circles = 7;
									    int helixes = 10;
									    int step = 0;
										for (int x = 0; x < circles; ++x) {
									        for (int i = 0; i < helixes; ++i) {
									            double angle = (double)step * radials + 6.283185307179586 * (double)i / (double)helixes;
									            Vector v = new Vector(Math.cos(angle) * (double)radius, (double)((float)step * grow), Math.sin(angle) * (double)radius);
									            VectorUtils.rotateAroundAxisX(v, (location.getPitch() + 90.0f) * 0.017453292f);
									            VectorUtils.rotateAroundAxisY(v, (- location.getYaw()) * 0.017453292f);
									            location.add(v);
									            world.spawnParticle(particlee, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, 0.0);
									            //display(particlee, location);
									            location.subtract(v);
									        }
									        ++step;
										}
										break;
									}
									case "warp": {
									    float radius = 1.0f;
									    EnumParticleTypes particlee = EnumParticleTypes.FIREWORKS_SPARK;
									    int particlees = 20;
									    float grow = 0.2f;
									    int rings = 12;
									    int step = 0;
									    if (step > rings) {
								            step = 0;
								        }
								        double y = (float)step * grow;
								        location.add(0.0, y, 0.0);
								        for (int i = 0; i < particlees; ++i) {
								            double angle = 6.283185307179586 * (double)i / (double)particlees;
								            double x = Math.cos(angle) * (double)radius;
								            double z = Math.sin(angle) * (double)radius;
								            location.add(x, 0.0, z);
							                world.spawnParticle(particlee, location.getX(), location.getY(), location.getZ(), particlees, 0, 0, 0, 0.0);
							                //display(particle, location);
								            location.subtract(x, 0.0, z);
								        }
								        location.subtract(0.0, y, 0.0);
								        ++step;
										break;
									}
									case "frost": {
										double t = 0; // t for Time
										double pi = Math.PI; // π (pi) is a very important concept in circular mathematics
										      										      
										t += pi / 16; // This adds 1/16 of pi to the time to give a circular increase
										                     // 1/16 of pi is in a unit called radians, it is equivalent to 22.5 degrees
										          
										for(double phi = 0; phi <= 2 * pi; phi += pi / 2) { // This for loop will spawn each particle for each circular rung
											double x = 0.3 * (4 * pi - t) * Math.cos(t + phi); // This and z define where in each rung the particle is spawned (notice this uses cosine())
											double y = 0.2 * t; // Notice this doesn't include phi, it could've been defined outside this for loop. This is because your y-value is your height and your height is the same for all particles in each rung of the circular ladder
											double z = 0.3 * (4 * pi - t) * Math.sin(t + phi); // This and x define where in each rung the particle is spawned (notice this uses sine())
											location.add(x,y,z); // This sets the location relative to the player's location
									        world.spawnParticle(EnumParticleTypes.SNOW_SHOVEL, location.getX(), location.getY(), location.getZ(), 50, 0, 0, 0, 1.0);
											//ParticleEffect.SNOW_SHOVEL.display(loc,0,0,0,0,1); //customize this for your particle library
									        location.subtract(x,y,z); // This resets the location without updating for where the player moved (That way it forms the circle and then moves relative to the player)
		
		
											if(t >= 4 * pi){ // Once there's a certain height, the little snowball explosion on top
											location.add(x,y,z);
											world.spawnParticle(EnumParticleTypes.SNOW_SHOVEL, location.getX(), location.getY(), location.getZ(), 50, 0, 0, 0, 1.0);
											//ParticleEffect.SNOW_SHOVEL.display(location,0,0,0,1,50); //customize this for your particle library
											// This is the little puff at top, just 50 snow shovel particles
											location.subtract(x,y,z);
											}
										}
										break;
									}
									default:{
							            break;
									}
								}
							}
							
						}
					}					
				}
			}
		}
	}

	protected void drawParticleDNA(Location location, Vector v, EnumParticleTypes particle, WorldServer world) {
        VectorUtils.rotateAroundAxisX(v, (location.getPitch() + 90) * MathUtils.degreesToRadians);
        VectorUtils.rotateAroundAxisY(v, -location.getYaw() * MathUtils.degreesToRadians);

        location.add(v);
        world.spawnParticle(particle, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, 0.0, 8);
        location.subtract(v);
    }
    
    protected void calculateSideRatio(float radius, float height, float sideRatio) {
        float grounds = 9.869605f * radius * 2.0f;
        float side = 6.2831855f * radius * height;
        sideRatio = side / (side + grounds);
    }
    
    public static enum Direction {
        UP,
        DOWN,
        BOTH;
        

        private Direction() {
        }
    }
    
    public void invalidate(boolean firstStep, Set<Vector> cacheGreen, Set<Vector> cacheBlue, int particlees, float radius, float mountainHeight) {
        firstStep = false;
        cacheGreen.clear();
        cacheBlue.clear();
        HashSet<Vector> cache = new HashSet<Vector>();
        int sqrtParticles = (int)Math.sqrt(particlees);
        float theta = 0.0f;
        float thetaStep = 3.1415927f / (float)sqrtParticles;
        float phiStep = 6.2831855f / (float)sqrtParticles;
        for (int i = 0; i < sqrtParticles; ++i) {
            theta += thetaStep;
            float phi = 0.0f;
            for (int j = 0; j < sqrtParticles; ++j) {
                float x = radius * MathUtils.sin(theta) * MathUtils.cos(phi += phiStep);
                float y = radius * MathUtils.sin(theta) * MathUtils.sin(phi);
                float z = radius * MathUtils.cos(theta);
                cache.add(new Vector(x, y, z));
            }
        }
        float increase = mountainHeight / (float)100;
        for (int i = 0; i < 100; ++i) {
            double r1 = RandomUtils.getRandomAngle();
            double r2 = RandomUtils.getRandomAngle();
            double r3 = RandomUtils.getRandomAngle();
            for (Vector v : cache) {
                if (v.getY() > 0.0) {
                    v.setY(v.getY() + (double)increase);
                } else {
                    v.setY(v.getY() - (double)increase);
                }
                if (i == 100 - 1) continue;
                VectorUtils.rotateVector(v, r1, r2, r3);
            }
        }
        float minSquared = Float.POSITIVE_INFINITY;
        float maxSquared = Float.NEGATIVE_INFINITY;
        for (Vector current : cache) {
            float lengthSquared = (float)current.lengthSquared();
            if (minSquared > lengthSquared) {
                minSquared = lengthSquared;
            }
            if (maxSquared >= lengthSquared) continue;
            maxSquared = lengthSquared;
        }
        float average = (minSquared + maxSquared) / 2.0f;
        for (Vector v : cache) {
            float lengthSquared = (float)v.lengthSquared();
            if (lengthSquared >= average) {
                cacheGreen.add(v);
                continue;
            }
            cacheBlue.add(v);
        }
    }
    
    protected void addParticle(Location location, Vector v, double rotation, EnumParticleTypes particle, WorldServer world) {
        v.setZ(0);
        VectorUtils.rotateAroundAxisY(v, rotation);
        location.add(v);
        world.spawnParticle(particle, location.getX(), location.getY(), location.getZ(), 1, 0, 0, 0, 5.0);
        //display(particle, location);
        location.subtract(v);
    }
    
    public ArrayList<Vector> createCircle(double y, double radius) {
        double amount = radius * 64.0;
        double inc = 6.283185307179586 / amount;
        ArrayList<Vector> vecs = new ArrayList<Vector>();
        int i = 0;
        while ((double)i < amount) {
            double angle = (double)i * inc;
            double x = radius * Math.cos(angle);
            double z = radius * Math.sin(angle);
            Vector v = new Vector(x, y, z);
            vecs.add(v);
            ++i;
        }
        return vecs;
    }
    
}
