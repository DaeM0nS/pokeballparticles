package fr.pokepixel.PokeballParticles.commands;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.collect.Lists;
import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.api.pokemon.Pokemon;
import com.pixelmonmod.pixelmon.storage.PlayerPartyStorage;

import fr.pixelmon_france.daem0ns.pokeballparticles.PokeballParticles;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class PokeballParticlesCommand extends CommandBase implements ICommand{
		private final List<String> aliases;


		public PokeballParticlesCommand(){
	        aliases = Lists.newArrayList("pokeballparticles", "pp");
	    }
		
		@Override
		@Nonnull
		public String getName() {
			return "pokeballparticles"; 
		}

		@Override
		@Nonnull
		public String getUsage(@Nonnull ICommandSender sender) {
			return "/pokeballparticles";
		}


		@Override
		@Nonnull
		public List<String> getAliases()
		{
			return aliases;
		}

		@Override
		public void execute(@Nonnull MinecraftServer server, @Nonnull ICommandSender sender, @Nonnull String[] args) throws CommandException  {

			if(sender instanceof EntityPlayerMP) {
				EntityPlayerMP player = (EntityPlayerMP) sender;
					if(args.length==0) {
						player.sendMessage(new TextComponentString("--------PokeballParticles------"));
						player.sendMessage(new TextComponentString("/pp -> Get this help message"));
						player.sendMessage(new TextComponentString("/pp list -> Get the list of all effects."));
						player.sendMessage(new TextComponentString("/pp world -> Get the list of worlds where you can see effects."));
						player.sendMessage(new TextComponentString("/pp reset <slot> -> Remove all effects on the ball."));
						player.sendMessage(new TextComponentString("/pp check <slot> -> Check what effects are on the selected ball."));
						player.sendMessage(new TextComponentString("/pp add <slot> <effect> -> Add the specified effect on the specified ball."));
						player.sendMessage(new TextComponentString("/pp set <slot> <effect> -> Set the specified effect on the specified ball."));
						player.sendMessage(new TextComponentString("/pp remove <slot> <effect> -> Remove the specified effect on the specified ball."));
						player.sendMessage(new TextComponentString("/pp reset <slot> -> Remove every effects on the specified ball."));
						player.sendMessage(new TextComponentString("--------PokeballParticles------"));
					}else if(args.length==1) {
						if(args[0].equalsIgnoreCase("list")){
							player.sendMessage(new TextComponentString("--------Availables effects------"));
							for(String effect:PokeballParticles.effects) {
								player.sendMessage(new TextComponentString("-> "+effect));
							}
							if(PokeballParticles.effects.isEmpty()){
								player.sendMessage(new TextComponentString("There is no effects at the moment .."));
							}
							return;
						}else 
						if(args[0].equalsIgnoreCase("world")){
							player.sendMessage(new TextComponentString("--------Availables worlds------"));
							for(Integer d:PokeballParticles.dims) {
								player.sendMessage(new TextComponentString("-> "+d));
							}
							if(PokeballParticles.effects.isEmpty()){
								player.sendMessage(new TextComponentString("There is no world at the moment .."));
							}
							return;
						}else
						if(args[0].equalsIgnoreCase("reset")){
							player.sendMessage(new TextComponentString("You must specify a valid slot."));
							return;
						}else
						if(args[0].equalsIgnoreCase("check")){
							player.sendMessage(new TextComponentString("You must specify a valid slot."));
							return;
						}else
						if(args[0].equalsIgnoreCase("add")){
							player.sendMessage(new TextComponentString("You must specify a valid slot and a valid effect."));
							return;
						}else
						if(args[0].equalsIgnoreCase("set")){
							player.sendMessage(new TextComponentString("You must specify a valid slot and a valid effect."));
							return;
						}
						else{
							getUsage(player);
							return;
						}
					}else if(args.length==2) {
						if(args[0].equalsIgnoreCase("reset")){
							if(isNumeric(args[1])){
								PlayerPartyStorage team = Pixelmon.storageManager.getParty(player);
								Pokemon[] equipe = team.getAll();
								if(equipe[Integer.parseInt(args[1])-1]!=null){
									NBTTagCompound pokemon = equipe[Integer.parseInt(args[1])-1].getPersistentData();
									player.sendMessage(new TextComponentString("Use the \"/pokeballparticles reset "+ args[1] + " confirm\" command to reset effects on the "+args[1]+" slot ball."));
									return;
								}else{
									player.sendMessage(new TextComponentString("This slot is empty."));
									return;									
								}
							}else{
								player.sendMessage(new TextComponentString("This slot is invalid."));
								return;
							}
						}else if(args[0].equalsIgnoreCase("check")){
							if(isNumeric(args[1])){
								PlayerPartyStorage team = Pixelmon.storageManager.getParty(player);
								Pokemon[] equipe = team.getAll();
								if(equipe[Integer.parseInt(args[1])-1]!=null){
									NBTTagCompound pokemon = equipe[Integer.parseInt(args[1])-1].getPersistentData();
									player.sendMessage(new TextComponentString("------PokeballParticles------"));
									if(!pokemon.hasKey("pokeballparticles")){
										player.sendMessage(new TextComponentString("-> There is no effect on this ball."));
										return;
									}else {
										if(pokemon.getString("pokeballparticles").isEmpty()){
											player.sendMessage(new TextComponentString("-> There is no effect on this ball."));
											return;
										}
										String[] Split = pokemon.getString("pokeballparticles").split(",");
										for( String effect:Split) {
												player.sendMessage(new TextComponentString("-> " + effect));
										}
										return;	
									}
								}else{
									player.sendMessage(new TextComponentString("This slot is empty."));
									return;									
								}
							}else{
								player.sendMessage(new TextComponentString("This slot is invalid."));
								return;
							}
						}else if(args[0].equalsIgnoreCase("add")){
							if(isNumeric(args[1])){
								player.sendMessage(new TextComponentString("You must specify an effect to apply on the pokeball."));
								return;
							}else{
								player.sendMessage(new TextComponentString("This slot is invalid."));
								return;
							}	
						}else if(args[0].equalsIgnoreCase("set")){
							if(isNumeric(args[1])){
								player.sendMessage(new TextComponentString("You must specify an effect to apply on the pokeball."));
								return;
							}else{
								player.sendMessage(new TextComponentString("This slot is invalid."));
								return;
							}	
						}else{
							getUsage(player);
							return;
						}
					}else if(args.length==3) {
						if(args[0].equalsIgnoreCase("reset")){
							if(isNumeric(args[1])){
								PlayerPartyStorage team = Pixelmon.storageManager.getParty(player);
								Pokemon[] equipe = team.getAll();
								if(equipe[Integer.parseInt(args[1])-1]!=null){
									NBTTagCompound pokemon = equipe[Integer.parseInt(args[1])-1].getPersistentData();
									if(args[2].equalsIgnoreCase("confirm")){
										if(!pokemon.hasKey("pokeballparticles")){
											player.sendMessage(new TextComponentString("There is no effect to reset on this ball"));
											return;
										}else{
											pokemon.removeTag("pokeballparticles");
											//pokemon.setString("pokeballparticles", "");
											player.sendMessage(new TextComponentString("Pokeball effects are succesfully reset for the slot "+ args[1] +"."));
											return;
										}
									}else{
										player.sendMessage(new TextComponentString("Use the \"/pokeballparticles reset "+ args[1] + " confirm\" command to reset effects on the "+args[1]+" slot ball."));
										return;
									}
								}else{
									player.sendMessage(new TextComponentString("This slot is empty."));
									return;									
								}
							}else{
								player.sendMessage(new TextComponentString("This slot is invalid."));
								return;
							}
						}else
							if(args[0].equalsIgnoreCase("add")){
								if(isNumericAnd16(args[1])){
									PlayerPartyStorage team = Pixelmon.storageManager.getParty(player);
									Pokemon[] equipe = team.getAll();
									if(equipe[Integer.parseInt(args[1])-1]!=null){
										NBTTagCompound pokemon = equipe[Integer.parseInt(args[1])-1].getPersistentData();
										if(PokeballParticles.effects.contains(args[2])){
											if(sender.canUseCommand(0, "pokeballparticles.command.use."+args[2].toLowerCase())){
												if(!pokemon.hasKey("pokeballparticles")){
													pokemon.setString("pokeballparticles", args[2]);
													player.sendMessage(new TextComponentString("You successfully add the "+ args[2] + " effect on the ball in the slot "+args[1]));
													return;
												}else {
													String[] Split = pokemon.getString("pokeballparticles").split(",");
													for( String effect:Split) {
														if(effect.equalsIgnoreCase(args[2])) {
															player.sendMessage(new TextComponentString("This effect is already on the ball"));
															return;
														}
													}
													pokemon.setString("pokeballparticles", pokemon.getString("pokeballparticles")+","+args[2]);
													player.sendMessage(new TextComponentString("You successfully add the "+ args[2] + " effect on the ball in the slot "+args[1]));
													return;	
												}	
											}else{
												player.sendMessage(new TextComponentString("You don't have permission to apply the "+ args[2] + " effect on the ball.."));
												return;
											}
										}else{
											player.sendMessage(new TextComponentString("This effect doesn't exist"));
											return;
										}
									}else {
										player.sendMessage(new TextComponentString("This slot is empty."));
										return;
									}
								}else {
									player.sendMessage(new TextComponentString("This slot is invalid."));
									return;
								}
							}else
								if(args[0].equalsIgnoreCase("set")){
									if(isNumericAnd16(args[1])){
										PlayerPartyStorage team = Pixelmon.storageManager.getParty(player);
										Pokemon[] equipe = team.getAll();
										if(equipe[Integer.parseInt(args[1])-1]!=null){
											NBTTagCompound pokemon = equipe[Integer.parseInt(args[1])-1].getPersistentData();
											if(PokeballParticles.effects.contains(args[2])){
												if(sender.canUseCommand(0, "pokeballparticles.command.use."+args[2].toLowerCase())){
													pokemon.setString("pokeballparticles", args[2]);
													player.sendMessage(new TextComponentString("You successfully set the "+ args[2] + " effect on the ball in the slot "+args[1]));
													return;
												}else{
													player.sendMessage(new TextComponentString("You don't have permission to apply the "+ args[2] + " effect on the ball.."));
													return;
												}
											}else{
												player.sendMessage(new TextComponentString("This effect doesn't exist"));
												return;
											}
										}else {
											player.sendMessage(new TextComponentString("This slot is empty."));
											return;
										}
									}else {
										player.sendMessage(new TextComponentString("This slot is invalid."));
										return;
									}
								}else
								if(args[0].equalsIgnoreCase("remove")){
									if(isNumericAnd16(args[1])){
										PlayerPartyStorage team = Pixelmon.storageManager.getParty(player);
										Pokemon[] equipe = team.getAll();
										if(equipe[Integer.parseInt(args[1])-1]!=null){
											NBTTagCompound pokemon = equipe[Integer.parseInt(args[1])-1].getPersistentData();
											if(PokeballParticles.effects.contains(args[2])){
												if(!pokemon.hasKey("pokeballparticles")){
													player.sendMessage(new TextComponentString("The "+ args[1] + " pokeball has no effect.."));
													return;
												}else {
													if(!pokemon.getString("pokeballparticles").contains(args[2])){
														player.sendMessage(new TextComponentString("The "+ args[1] + " pokeball has no the "+ args[2] +" effect.."));
														return;
													}
													String[] Split = pokemon.getString("pokeballparticles").split(",");
													String effects = "";
													for(String effet : Split){
														if(!effet.equalsIgnoreCase(args[2])){
															if(effects.isEmpty()){
																effects=effet;
															}else{
																effects=effects+","+effet;
															}
														}
													}
													if(effects.isEmpty()){
														pokemon.removeTag("pokeballparticles");
													}else{
														pokemon.setString("pokeballparticles", effects);
													}
													player.sendMessage(new TextComponentString("You successfully remove the "+args[2] + "effect of the pokeball "+ args[1]));
													return;	
												}
											}else{
												player.sendMessage(new TextComponentString("This effect doesn't exist"));
												return;
											}
										}else {
											player.sendMessage(new TextComponentString("This slot is empty."));
											return;
										}
									}else {
										player.sendMessage(new TextComponentString("This slot is invalid."));
										return;
									}
								}
							else{
							getUsage(player);
							return;
						}					
					}
			}else {
				
			}
		}

		@Override
		@Nonnull
	    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos pos) {
			String[] particles = null;
			ArrayList<String> effects = new ArrayList<String>();
			if(args.length==1){
				return getListOfStringsMatchingLastWord(args, new String[] {"list", "add", "check", "remove", "reset", "set"});
			}else
			if(args.length==2){
				if(args[0].equalsIgnoreCase("add") || args[0].equalsIgnoreCase("set") || args[0].equalsIgnoreCase("check") || args[0].equalsIgnoreCase("reset") || args[0].equalsIgnoreCase("remove")){
					return getListOfStringsMatchingLastWord(args, new String[] {"1", "2", "3", "4", "5", "6"});
				}else{
					return null;
				}
			}else
			if(args.length==3){
				if(args[0].equalsIgnoreCase("set")){
					for(String effect : PokeballParticles.effects) {
						if(sender.canUseCommand(0, "pokeballparticles.command.use."+effect)) {
							effects.add(effect);
						}
					}
					return getListOfStringsMatchingLastWord(args, effects);
				}else
				if(args[0].equalsIgnoreCase("add")){
					for(String effect : PokeballParticles.effects) {
						if(sender.canUseCommand(0, "pokeballparticles.command.use."+effect)) {
							effects.add(effect);
						}
					}
					return getListOfStringsMatchingLastWord(args, effects);
				}else
				if(args[0].equalsIgnoreCase("reset")){
					return getListOfStringsMatchingLastWord(args, new String[] {"confirm"});
				}else
				if(args[0].equalsIgnoreCase("remove")){
					PlayerPartyStorage team = Pixelmon.storageManager.getParty(FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUsername(sender.getName()));
					Pokemon[] equipe = team.getAll();
					if(equipe[Integer.parseInt(args[1])-1]!=null){
						NBTTagCompound pokemon = equipe[Integer.parseInt(args[1])-1].getPersistentData();
						if(pokemon.hasKey("pokeballparticles") || !pokemon.getString("pokeballparticles").isEmpty()) {
							if(pokemon.getString("pokeballparticles").contains(",")) {
								particles = pokemon.getString("pokeballparticles").split(",");
								return getListOfStringsMatchingLastWord(args, particles);									
							}else {
								return getListOfStringsMatchingLastWord(args, new String[] {pokemon.getString("pokeballparticles")});
							}
						}else {
							return null;
						}
					}else {
						return null;
					}
				}else{
					return null;
				}
			}
		    return null;
	
	    }

		@Override
		public boolean isUsernameIndex(String[] astring, int i)
		{
			return false;
		}
		public static boolean isNumericAnd16(String str)  
		{  
		  try  
		  {  
		    double d = Double.parseDouble(str);  
		  if(d>0){
			  if(d<7){
				  return true;
			  }else{
				  return false;
			  }
		  }else{
			  return false;
		  }
		  }  
		  catch(NumberFormatException nfe)  
		  {  
		    return false;  
		  }  
		}
		
		public static boolean isNumeric(String str)  
		{  
		  try  
		  {  
		    int d = Integer.parseInt(str);  
			return true;
		  }  
		  catch(NumberFormatException nfe)  
		  {  
		    return false;  
		  }  
		}	
	}
