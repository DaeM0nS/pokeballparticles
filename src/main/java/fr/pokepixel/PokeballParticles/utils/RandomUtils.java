/*
 * Decompiled with CFR 0_122.
 * 
 * Could not load the following classes:
 *  org.bukkit.Material
 *  org.bukkit.util.Vector
 */
package fr.pokepixel.PokeballParticles.utils;

import java.util.Random;

public final class RandomUtils {
    public static final Random random = new Random(System.nanoTime());

    private RandomUtils() {
    }

    public static Vector getRandomVector() {
        double x = random.nextDouble() * 2.0 - 1.0;
        double y = random.nextDouble() * 2.0 - 1.0;
        double z = random.nextDouble() * 2.0 - 1.0;
        return new Vector(x, y, z).normalize();
    }

    public static Vector getRandomCircleVector() {
        double rnd = random.nextDouble() * 2.0 * 3.141592653589793;
        double x = Math.cos(rnd);
        double z = Math.sin(rnd);
        return new Vector(x, 0.0, z);
    }

  /*  public static Material getRandomMaterial(Material[] materials) {
        return materials[random.nextInt(materials.length)];
    }
*/
    public static double getRandomAngle() {
        return random.nextDouble() * 2.0 * 3.141592653589793;
    }
}

