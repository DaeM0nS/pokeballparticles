/*
 * Decompiled with CFR 0_122.
 * 
 * Could not load the following classes:
 *  org.bukkit.Location
 *  org.bukkit.Particle
 */
package de.slikey.effectlib.effect;

import de.slikey.effectlib.Effect;
import de.slikey.effectlib.EffectManager;
import de.slikey.effectlib.EffectType;
import org.bukkit.Location;
import org.bukkit.Particle;

public class MusicEffect
extends Effect {
    public Particle particle = Particle.NOTE;
    public double radialsPerStep = 0.39269908169872414;
    public float radius = 0.4f;
    protected float step = 0.0f;

    public MusicEffect(EffectManager effectManager) {
        super(effectManager);
        this.type = EffectType.REPEATING;
        this.iterations = 400;
        this.period = 1;
    }

    @Override
    public void reset() {
        this.step = 0.0f;
    }

    @Override
    public void onRun() {
        Location location = this.getLocation();
        location.add(0.0, 1.899999976158142, 0.0);
        location.add(Math.cos(this.radialsPerStep * (double)this.step) * (double)this.radius, 0.0, Math.sin(this.radialsPerStep * (double)this.step) * (double)this.radius);
        this.display(this.particle, location);
        this.step += 1.0f;
    }
}

