/*
 * Decompiled with CFR 0_122.
 */
package de.slikey.effectlib.effect;

import de.slikey.effectlib.EffectManager;
import de.slikey.effectlib.effect.JumpEffect;

public class SkyRocketEffect
extends JumpEffect {
    public SkyRocketEffect(EffectManager effectManager) {
        super(effectManager);
        this.power = 10.0f;
    }
}

