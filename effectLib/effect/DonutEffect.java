/*
 * Decompiled with CFR 0_122.
 * 
 * Could not load the following classes:
 *  org.bukkit.Location
 *  org.bukkit.Particle
 *  org.bukkit.util.Vector
 */
package de.slikey.effectlib.effect;

import de.slikey.effectlib.Effect;
import de.slikey.effectlib.EffectManager;
import de.slikey.effectlib.EffectType;
import de.slikey.effectlib.util.VectorUtils;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.util.Vector;

public class DonutEffect
extends Effect {
    public Particle particle = Particle.FLAME;
    public int particlesCircle = 10;
    public int circles = 36;
    public float radiusDonut = 2.0f;
    public float radiusTube = 0.5f;
    public double xRotation;
    public double yRotation;
    public double zRotation = 0.0;

    public DonutEffect(EffectManager effectManager) {
        super(effectManager);
        this.type = EffectType.REPEATING;
        this.period = 10;
        this.iterations = 20;
    }

    @Override
    public void onRun() {
        Location location = this.getLocation();
        Vector v = new Vector();
        for (int i = 0; i < this.circles; ++i) {
            double theta = 6.283185307179586 * (double)i / (double)this.circles;
            for (int j = 0; j < this.particlesCircle; ++j) {
                double phi = 6.283185307179586 * (double)j / (double)this.particlesCircle;
                double cosPhi = Math.cos(phi);
                v.setX(((double)this.radiusDonut + (double)this.radiusTube * cosPhi) * Math.cos(theta));
                v.setY(((double)this.radiusDonut + (double)this.radiusTube * cosPhi) * Math.sin(theta));
                v.setZ((double)this.radiusTube * Math.sin(phi));
                VectorUtils.rotateVector(v, this.xRotation, this.yRotation, this.zRotation);
                this.display(this.particle, location.add(v));
                location.subtract(v);
            }
        }
    }
}

