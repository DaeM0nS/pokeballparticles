/*
 * Decompiled with CFR 0_122.
 * 
 * Could not load the following classes:
 *  org.bukkit.Location
 *  org.bukkit.Particle
 *  org.bukkit.util.Vector
 */
package de.slikey.effectlib.effect;

import de.slikey.effectlib.Effect;
import de.slikey.effectlib.EffectManager;
import de.slikey.effectlib.EffectType;
import de.slikey.effectlib.util.VectorUtils;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.util.Vector;

public class VortexEffect
extends Effect {
    public Particle particle = Particle.FLAME;
    public float radius = 2.0f;
    public float grow = 0.05f;
    public double radials = 0.19634954084936207;
    public int circles = 3;
    public int helixes = 4;
    protected int step = 0;

    public VortexEffect(EffectManager effectManager) {
        super(effectManager);
        this.type = EffectType.REPEATING;
        this.period = 1;
        this.iterations = 200;
    }

    @Override
    public void reset() {
        this.step = 0;
    }

    @Override
    public void onRun() {
        Location location = this.getLocation();
        for (int x = 0; x < this.circles; ++x) {
            for (int i = 0; i < this.helixes; ++i) {
                double angle = (double)this.step * this.radials + 6.283185307179586 * (double)i / (double)this.helixes;
                Vector v = new Vector(Math.cos(angle) * (double)this.radius, (double)((float)this.step * this.grow), Math.sin(angle) * (double)this.radius);
                VectorUtils.rotateAroundAxisX(v, (location.getPitch() + 90.0f) * 0.017453292f);
                VectorUtils.rotateAroundAxisY(v, (- location.getYaw()) * 0.017453292f);
                location.add(v);
                this.display(this.particle, location);
                location.subtract(v);
            }
            ++this.step;
        }
    }
}

