/*
 * Decompiled with CFR 0_122.
 * 
 * Could not load the following classes:
 *  org.bukkit.Location
 *  org.bukkit.Particle
 *  org.bukkit.util.Vector
 */
package de.slikey.effectlib.effect;

import de.slikey.effectlib.Effect;
import de.slikey.effectlib.EffectManager;
import de.slikey.effectlib.EffectType;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.util.Vector;

public class LineEffect
extends Effect {
    public Particle particle = Particle.FLAME;
    public boolean isZigZag = false;
    public int zigZags = 10;
    public Vector zigZagOffset = new Vector(0.0, 0.1, 0.0);
    public int particles = 100;
    public double length = 0.0;
    protected boolean zag = false;
    protected int step = 0;

    public LineEffect(EffectManager effectManager) {
        super(effectManager);
        this.type = EffectType.REPEATING;
        this.period = 1;
        this.iterations = 1;
    }

    @Override
    public void reset() {
        this.step = 0;
    }

    @Override
    public void onRun() {
        Location location = this.getLocation();
        Location target = null;
        target = this.length > 0.0 ? location.clone().add(location.getDirection().normalize().multiply(this.length)) : this.getTarget();
        int amount = this.particles / this.zigZags;
        if (target == null) {
            this.cancel();
            return;
        }
        Vector link = target.toVector().subtract(location.toVector());
        float length = (float)link.length();
        link.normalize();
        float ratio = length / (float)this.particles;
        Vector v = link.multiply(ratio);
        Location loc = location.clone().subtract(v);
        for (int i = 0; i < this.particles; ++i) {
            if (this.isZigZag) {
                if (this.zag) {
                    loc.add(this.zigZagOffset);
                } else {
                    loc.subtract(this.zigZagOffset);
                }
            }
            if (this.step >= amount) {
                this.zag = !this.zag;
                this.step = 0;
            }
            ++this.step;
            loc.add(v);
            this.display(this.particle, loc);
        }
    }
}

