/*
 * Decompiled with CFR 0_122.
 * 
 * Could not load the following classes:
 *  org.bukkit.Location
 *  org.bukkit.Particle
 *  org.bukkit.util.Vector
 */
package de.slikey.effectlib.effect;

import de.slikey.effectlib.Effect;
import de.slikey.effectlib.EffectManager;
import de.slikey.effectlib.EffectType;
import de.slikey.effectlib.util.VectorUtils;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.util.Vector;

public class CircleEffect
extends Effect {
    public Particle particle = Particle.VILLAGER_HAPPY;
    public double xRotation;
    public double yRotation;
    public double zRotation = 0.0;
    public double angularVelocityX = 0.015707963267948967;
    public double angularVelocityY = 0.018479956785822312;
    public double angularVelocityZ = 0.02026833970057931;
    public float radius = 0.4f;
    protected float step = 0.0f;
    public double xSubtract;
    public double ySubtract;
    public double zSubtract;
    public boolean enableRotation = true;
    public int particles = 20;
    public boolean wholeCircle = false;

    public CircleEffect(EffectManager effectManager) {
        super(effectManager);
        this.type = EffectType.REPEATING;
        this.period = 2;
        this.iterations = 50;
    }

    @Override
    public void reset() {
        this.step = 0.0f;
    }

    @Override
    public void onRun() {
        Location location = this.getLocation();
        location.subtract(this.xSubtract, this.ySubtract, this.zSubtract);
        double inc = 6.283185307179586 / (double)this.particles;
        int steps = this.wholeCircle ? this.particles : 1;
        for (int i = 0; i < steps; ++i) {
            double angle = (double)this.step * inc;
            Vector v = new Vector();
            v.setX(Math.cos(angle) * (double)this.radius);
            v.setZ(Math.sin(angle) * (double)this.radius);
            VectorUtils.rotateVector(v, this.xRotation, this.yRotation, this.zRotation);
            if (this.enableRotation) {
                VectorUtils.rotateVector(v, this.angularVelocityX * (double)this.step, this.angularVelocityY * (double)this.step, this.angularVelocityZ * (double)this.step);
            }
            this.display(this.particle, location.clone().add(v), 0.0f, 30);
            this.step += 1.0f;
        }
    }
}

