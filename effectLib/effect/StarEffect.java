/*
 * Decompiled with CFR 0_122.
 * 
 * Could not load the following classes:
 *  org.bukkit.Location
 *  org.bukkit.Particle
 *  org.bukkit.util.Vector
 */
package de.slikey.effectlib.effect;

import de.slikey.effectlib.Effect;
import de.slikey.effectlib.EffectManager;
import de.slikey.effectlib.EffectType;
import de.slikey.effectlib.util.RandomUtils;
import de.slikey.effectlib.util.VectorUtils;
import java.util.Random;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.util.Vector;

public class StarEffect
extends Effect {
    public Particle particle = Particle.FLAME;
    public int particles = 50;
    public float spikeHeight = 3.5f;
    public int spikesHalf = 3;
    public float innerRadius = 0.5f;

    public StarEffect(EffectManager effectManager) {
        super(effectManager);
        this.type = EffectType.REPEATING;
        this.period = 4;
        this.iterations = 50;
    }

    @Override
    public void onRun() {
        Location location = this.getLocation();
        float radius = 3.0f * this.innerRadius / 1.73205f;
        for (int i = 0; i < this.spikesHalf * 2; ++i) {
            double xRotation = (double)i * 3.141592653589793 / (double)this.spikesHalf;
            for (int x = 0; x < this.particles; ++x) {
                double angle = 6.283185307179586 * (double)x / (double)this.particles;
                float height = RandomUtils.random.nextFloat() * this.spikeHeight;
                Vector v = new Vector(Math.cos(angle), 0.0, Math.sin(angle));
                v.multiply((this.spikeHeight - height) * radius / this.spikeHeight);
                v.setY(this.innerRadius + height);
                VectorUtils.rotateAroundAxisX(v, xRotation);
                location.add(v);
                this.display(this.particle, location);
                location.subtract(v);
                VectorUtils.rotateAroundAxisX(v, 3.141592653589793);
                VectorUtils.rotateAroundAxisY(v, 1.5707963267948966);
                location.add(v);
                this.display(this.particle, location);
                location.subtract(v);
            }
        }
    }
}

