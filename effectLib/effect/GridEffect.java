/*
 * Decompiled with CFR 0_122.
 * 
 * Could not load the following classes:
 *  org.bukkit.Location
 *  org.bukkit.Particle
 *  org.bukkit.util.Vector
 */
package de.slikey.effectlib.effect;

import de.slikey.effectlib.Effect;
import de.slikey.effectlib.EffectManager;
import de.slikey.effectlib.EffectType;
import de.slikey.effectlib.util.VectorUtils;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.util.Vector;

public class GridEffect
extends Effect {
    public Particle particle = Particle.FLAME;
    public int rows = 5;
    public int columns = 10;
    public float widthCell = 1.0f;
    public float heightCell = 1.0f;
    public int particlesWidth = 4;
    public int particlesHeight = 3;
    public double rotation = 0.0;

    public GridEffect(EffectManager effectManager) {
        super(effectManager);
        this.type = EffectType.INSTANT;
        this.period = 5;
        this.iterations = 50;
    }

    @Override
    public void onRun() {
        int j;
        int i;
        Location location = this.getLocation();
        Vector v = new Vector();
        for (i = 0; i <= this.rows + 1; ++i) {
            for (j = 0; j < this.particlesWidth * (this.columns + 1); ++j) {
                v.setY((float)i * this.heightCell);
                v.setX((float)j * this.widthCell / (float)this.particlesWidth);
                this.addParticle(location, v);
            }
        }
        for (i = 0; i <= this.columns + 1; ++i) {
            for (j = 0; j < this.particlesHeight * (this.rows + 1); ++j) {
                v.setX((float)i * this.widthCell);
                v.setY((float)j * this.heightCell / (float)this.particlesHeight);
                this.addParticle(location, v);
            }
        }
    }

    protected void addParticle(Location location, Vector v) {
        v.setZ(0);
        VectorUtils.rotateAroundAxisY(v, this.rotation);
        location.add(v);
        this.display(this.particle, location);
        location.subtract(v);
    }
}

