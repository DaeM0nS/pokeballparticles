/*
 * Decompiled with CFR 0_122.
 * 
 * Could not load the following classes:
 *  org.bukkit.Color
 *  org.bukkit.Location
 *  org.bukkit.Particle
 *  org.bukkit.util.Vector
 */
package de.slikey.effectlib.effect;

import de.slikey.effectlib.Effect;
import de.slikey.effectlib.EffectManager;
import de.slikey.effectlib.EffectType;
import de.slikey.effectlib.util.RandomUtils;
import java.util.ArrayList;
import java.util.Random;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.util.Vector;

public class TornadoEffect
extends Effect {
    public Particle tornadoParticle = Particle.FLAME;
    public Color tornadoColor = null;
    public Particle cloudParticle = Particle.CLOUD;
    public Color cloudColor = null;
    public float cloudSize = 2.5f;
    public double yOffset = 0.8;
    public float tornadoHeight = 5.0f;
    public float maxTornadoRadius = 5.0f;
    public boolean showCloud = true;
    public boolean showTornado = true;
    public double distance = 0.375;
    protected int step = 0;

    public TornadoEffect(EffectManager manager) {
        super(manager);
        this.type = EffectType.REPEATING;
        this.period = 5;
        this.iterations = 20;
    }

    @Override
    public void reset() {
        this.step = 0;
    }

    @Override
    public void onRun() {
        Location l = this.getLocation().add(0.0, this.yOffset, 0.0);
        int i = 0;
        while ((float)i < 100.0f * this.cloudSize) {
            Vector v = RandomUtils.getRandomCircleVector().multiply(RandomUtils.random.nextDouble() * (double)this.cloudSize);
            if (this.showCloud) {
                this.display(this.cloudParticle, l.add(v), this.cloudColor, 0.0f, 7);
                l.subtract(v);
            }
            ++i;
        }
        Location t = l.clone().add(0.0, 0.2, 0.0);
        double r = 0.45 * ((double)this.maxTornadoRadius * (2.35 / (double)this.tornadoHeight));
        for (double y = 0.0; y < (double)this.tornadoHeight; y += this.distance) {
            double fr = r * y;
            if (fr > (double)this.maxTornadoRadius) {
                fr = this.maxTornadoRadius;
            }
            for (Vector v : this.createCircle(y, fr)) {
                if (!this.showTornado) continue;
                this.display(this.tornadoParticle, t.add(v), this.tornadoColor);
                t.subtract(v);
                ++this.step;
            }
        }
        l.subtract(0.0, this.yOffset, 0.0);
    }

    public ArrayList<Vector> createCircle(double y, double radius) {
        double amount = radius * 64.0;
        double inc = 6.283185307179586 / amount;
        ArrayList<Vector> vecs = new ArrayList<Vector>();
        int i = 0;
        while ((double)i < amount) {
            double angle = (double)i * inc;
            double x = radius * Math.cos(angle);
            double z = radius * Math.sin(angle);
            Vector v = new Vector(x, y, z);
            vecs.add(v);
            ++i;
        }
        return vecs;
    }
}

